package com.homework.zoo;

/**
 * Created by Valeriy on 21.10.2016.
 */
public class Shark extends Animal {
    Shark(String name){
        this.name=name;
        this.animalType=AnimalType.FISH;
    }
    @Override
    void sayName() {
        System.out.println("I am a "+this.animalType+". My name is "+this.name);
    }
}
