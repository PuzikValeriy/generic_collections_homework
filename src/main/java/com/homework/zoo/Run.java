package com.homework.zoo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valeriy on 21.10.2016.
 */

public class Run {
    public static void main(String[] args) {
        Bear bear = new Bear("Grylls");
        Monkey monkey = new Monkey("Mokky");
        Shark shark = new Shark("Sharky");
        Zebra zebra = new Zebra("Striped");

        ZooBox<Bear> bearZooBox= new ZooBox<>();
        bearZooBox.lockAnimal(bear);
        bearZooBox.getAnimal().sayName();
        ZooBox<Monkey> monkeyZooBox=new ZooBox<>();
        monkeyZooBox.lockAnimal(monkey);
        monkeyZooBox.getAnimal().sayName();
        ZooBox<Shark> sharkZooBox = new ZooBox<>();
        sharkZooBox.lockAnimal(shark);
        sharkZooBox.getAnimal().sayName();
        ZooBox<Zebra> zebraZooBox  =new ZooBox<>();
        zebraZooBox.lockAnimal(zebra);
        zebraZooBox.getAnimal().sayName();
        System.out.println();

        ZooBox<Animal> zooBox = new ZooBox<>();
        zooBox.lockAnimal(bear);
        zooBox.lockAnimal(monkey);
        zooBox.lockAnimal(shark);
        zooBox.lockAnimal(zebra);
        zooBox.getAnimal().sayName();
        System.out.println();

        List<ZooBox> zoo = new ArrayList<>();
        zoo.add(bearZooBox);
        zoo.add(monkeyZooBox);
        zoo.add(sharkZooBox);
        zoo.add(zebraZooBox);
        for(ZooBox animal:zoo){
            animal.getAnimal().sayName();
        }
    }
}
