package com.homework.zoo;

/**
 * Created by Valeriy on 21.10.2016.
 */
public abstract class Animal {
    AnimalType animalType;
    String name;
    abstract void sayName();
}
