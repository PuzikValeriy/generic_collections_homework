package com.homework.zoo;

/**
 * Created by Valeriy on 21.10.2016.
 */
public class Bear extends Animal {
    Bear(String name){
        this.name=name;
        this.animalType=AnimalType.MAMMAL;
    }
    @Override
    void sayName() {
        System.out.println("I am a "+this.animalType+". My name is "+this.name);
    }
}
