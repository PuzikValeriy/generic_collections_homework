package com.homework.sport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valeriy on 21.10.2016.
 */
public class Run {
    public static void main(String[] args) {
        Football football = new Football("Football","Game description");
        Hockey hockey = new Hockey("Hockey","Game description");
        Formula formula = new Formula("Formula","Game description");

        Ball ball = new Ball("Ball");
        Car car = new Car("Car");
        Fans fans = new Fans("Fans");
        Ice ice = new Ice("Ice");

        List<Constituent> footballConstituents = new ArrayList<>();
        footballConstituents.add(ball);
        footballConstituents.add(fans);
        List<Constituent> hockeyConstituents = new ArrayList<>();
        hockeyConstituents.add(ice);
        hockeyConstituents.add(fans);
        List<Constituent> formulaConstituents = new ArrayList<>();
        formulaConstituents.add(car);
        formulaConstituents.add(fans);
        Sport<Football,Constituent> footballBallSport = new Sport<>(football,footballConstituents);
        footballBallSport.cantLiveWithout();
        Sport<Hockey,Constituent> hockeyBallSport = new Sport<>(hockey,hockeyConstituents);
        hockeyBallSport.cantLiveWithout();
        Sport<Formula,Constituent> formulaBallSport = new Sport<>(formula,formulaConstituents);
        formulaBallSport.cantLiveWithout();
    }
}
