package com.homework.sport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Valeriy on 21.10.2016.
 */
public class Sport<K extends Discipline,V extends Constituent> {
    private K discipline;
    private List<V> constituents = new ArrayList<>();
    Sport(K discipline,List<V> constituents){
        this.discipline=discipline;
        this.constituents=constituents;
    }

    void cantLiveWithout(){
        System.out.print(discipline.description()+". It can't live without ");
        Iterator<V> iterator = constituents.iterator();
        while (iterator.hasNext()){
            V i = iterator.next();
            System.out.print(i.about());
            if(iterator.hasNext())
            System.out.print(" and ");
        }
        System.out.println(".");
    }
}
